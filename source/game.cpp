#include "GV.h"

//ゲームクリアチェック
void searchend(){
	int i;
	for(i=0;i<divnum*divnum;i++){
		if(puzzlenum[i]!=i){
			return;
		}
	}

	func_state=120;
}


//ゲームの基本状態
void puzzlegame(){
	int i,j;
	int tmp;

	DrawExtendGraph( 0 , 0 , 640 , 480 , img_board[0] , FALSE ) ;
	DrawRotaGraph( 520 , 80 , 0.5f , 0 , img_board[13]  , TRUE ) ;
	DrawRotaGraph( 520 , 180 , 0.5f , 0 , img_board[14]  , TRUE ) ;
	DrawRotaGraph( 520 , 280 , 0.5f , 0 , img_board[15]  , TRUE ) ;



	//画像の表示
	for(j=0;j<divnum;j++){
		for(i=0;i<divnum;i++){
			if(i+j*divnum==space){
				continue;
			}
			DrawExtendGraph( 30+(360/divnum)*i, 50+(360/divnum)*j , 30+(360/divnum)*(i+1) ,50+(360/divnum)*(j+1) , puzzlegraph[puzzlenum[i+divnum*j]] , FALSE) ;
		}
	}
	if(space!=divnum*divnum){
			DrawExtendGraph( 30+(360/divnum)*divnum, 50+(360/divnum)*(divnum-1) , 30+(360/divnum)*(divnum+1) ,50+(360/divnum)*divnum , puzzlegraph[puzzlenum[divnum*divnum]] , FALSE) ;		
	}


	//外枠 360*360
	DrawBox( 30 , 50 , 390 , 410, color[3] , FALSE) ;
	for(i=0;i<divnum;i++){
		for(j=0;j<divnum;j++){
			DrawBox( 30+(360/divnum)*i, 50+(360/divnum)*j , 30+(360/divnum)*(i+1) ,50+(360/divnum)*(j+1) , color[3] , FALSE) ;
		}
	}
	DrawBox( 389, 409-(360/divnum) , 389+(360/divnum) , 410, color[3] , FALSE) ;
	
	//66 33 スイッチ 115 33 520

	//クリック
	if(CheckStateMouse(MouseLeft)==1){
		//プレビュー
		if(Mousesquare(405 , 47 , 635 , 113)==1){
			PlaySoundMem( sound_se[1] , DX_PLAYTYPE_BACK ) ;
			game_state=2;
		}
		//hint
		else if(Mousesquare(405 , 147 , 635 ,213)==1){
			PlaySoundMem( sound_se[1] , DX_PLAYTYPE_BACK ) ;
			game_state=1;
		}
		//降参
		else if(Mousesquare(454 , 247 , 586 ,313)==1){
			PlaySoundMem( sound_se[1] , DX_PLAYTYPE_BACK ) ;
			func_state=1;
		}


		//ゲーム
		//Mousesquare(30+(360/divnum)*(space%divnum), 50+(360/divnum)*(space/divnum) , 30+(360/divnum)*((space%divnum)+1) ,50+(360/divnum)*((space/divnum)+1))
		if(space!=divnum*divnum){//特別枠じゃない
			//空白の上
			if(Mousesquare(30+(360/divnum)*(space%divnum), 50+(360/divnum)*((space/divnum)-1) , 30+(360/divnum)*((space%divnum)+1) ,50+(360/divnum)*(space/divnum))==1){
				if(space/divnum != 0){
					PlaySoundMem( sound_se[1] , DX_PLAYTYPE_BACK ) ;
					puzzlenum[space]=puzzlenum[space-divnum];
					space=space-divnum;
				}
			}
			//左
			else if(Mousesquare(30+(360/divnum)*((space%divnum)-1), 50+(360/divnum)*(space/divnum) , 30+(360/divnum)*(space%divnum) ,50+(360/divnum)*((space/divnum)+1))==1){
				if(space%divnum!=0){
					PlaySoundMem( sound_se[1] , DX_PLAYTYPE_BACK ) ;
					puzzlenum[space]=puzzlenum[space-1];
					space=space-1;
				}
			}	
			//右
			else if(Mousesquare(30+(360/divnum)*((space%divnum)+1), 50+(360/divnum)*(space/divnum) , 30+(360/divnum)*((space%divnum)+2) ,50+(360/divnum)*((space/divnum)+1))==1){
				if( space%divnum!= divnum-1 || space==divnum*divnum-1){//右がある
					PlaySoundMem( sound_se[1] , DX_PLAYTYPE_BACK ) ;
					puzzlenum[space]=puzzlenum[space+1];
					space=space+1;
				}

			}	
			//下
			else if(Mousesquare(30+(360/divnum)*(space%divnum), 50+(360/divnum)*((space/divnum)+1) , 30+(360/divnum)*((space%divnum)+1) ,50+(360/divnum)*((space/divnum)+2))==1){
				if(space/divnum !=divnum-1){
					PlaySoundMem( sound_se[1] , DX_PLAYTYPE_BACK ) ;
					puzzlenum[space]=puzzlenum[space+divnum];
					space=space+divnum;
				}
			}
		}
		else{//特別枠
			if(Mousesquare(389-(360/divnum), 409-(360/divnum) , 389 , 410)==1){
				PlaySoundMem( sound_se[1] , DX_PLAYTYPE_BACK ) ;
				puzzlenum[space]=puzzlenum[space-1];
				space=space-1;
			}
		}

	}

	searchend();
	
}

void hint(){
	int i,j;
	DrawExtendGraph( 0 , 0 , 640 , 480 , img_board[0] , FALSE ) ;
	DrawRotaGraph( 520 , 80 , 0.5f , 0 , img_board[13]  , TRUE ) ;
	DrawRotaGraph( 520 , 180 , 0.5f , 0 , img_board[14]  , TRUE ) ;
	DrawRotaGraph( 520 , 280 , 0.5f , 0 , img_board[15]  , TRUE ) ;



	//画像の表示
	SetFontSize( 240/divnum ) ;
	for(j=0;j<divnum;j++){
		for(i=0;i<divnum;i++){
			if(i+j*divnum==space){
				continue;
			}
			DrawExtendGraph( 30+(360/divnum)*i, 50+(360/divnum)*j , 30+(360/divnum)*(i+1) ,50+(360/divnum)*(j+1) , puzzlegraph[puzzlenum[i+divnum*j]] , FALSE) ;
			DrawFormatString( 30+(360/divnum)*i, 50+(360/divnum)*j ,color[2],"%d",puzzlenum[i+divnum*j]) ;

		}
	}
	if(space!=divnum*divnum){
			DrawExtendGraph( 30+(360/divnum)*divnum, 50+(360/divnum)*(divnum-1) , 30+(360/divnum)*(divnum+1) ,50+(360/divnum)*divnum , puzzlegraph[puzzlenum[divnum*divnum]] , FALSE) ;		
			DrawFormatString( 30+(360/divnum)*divnum, 50+(360/divnum)*(divnum-1) ,color[2],"%d",puzzlenum[divnum*divnum]) ;
	}
	SetFontSize( FontSize) ;	

	//外枠 360*360
	DrawBox( 30 , 50 , 390 , 410, color[3] , FALSE) ;
	for(i=0;i<divnum;i++){
		for(j=0;j<divnum;j++){
			DrawBox( 30+(360/divnum)*i, 50+(360/divnum)*j , 30+(360/divnum)*(i+1) ,50+(360/divnum)*(j+1) , color[3] , FALSE) ;
		}
	}
	DrawBox( 389, 409-(360/divnum) , 389+(360/divnum) , 410, color[3] , FALSE) ;


	//クリック
	if(CheckStateMouse(MouseLeft)==1){
		game_state=0;
	}


}

void preview(){
	DrawExtendGraph( 0 , 0 , 640 , 480 , graphfile[pointer] , FALSE ) ;
	//クリック
	if(CheckStateMouse(MouseLeft)==1){
		game_state=0;
	}
}

void game(){
	switch(game_state){
	case 0:
		puzzlegame();		
		break;
	case 1:
		hint();
		break;
	case 2:
		preview();
		break;
	case 3:
		break;
	}
}