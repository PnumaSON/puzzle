#define GLOBAL_INSTANCE 
#include "GV.h"

//ループで必ず行う３大処理
int ProcessLoop(){
    if(ProcessMessage()!=0)return -1;//プロセス処理がエラーなら-1を返す
    if(ClearDrawScreen()!=0)return -1;//画面クリア処理がエラーなら-1を返す
    GetHitKeyStateAll_2();//現在のキー入力処理を行う
    GetHitPadStateAll();  //現在のパッド入力処理を行う
	GetMouseState();
	GetMousePoint( &MouseX , &MouseY ) ;
    return 0;
}

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow){
    ChangeWindowMode(TRUE);//ウィンドウモード
	SetMainWindowText( "パズルゲー" ) ;
	SetOutApplicationLogValidFlag( FALSE ) ;
    if(DxLib_Init() == -1 || SetDrawScreen( DX_SCREEN_BACK )!=0) return -1;//初期化と裏画面化

    while(ProcessLoop()==0){//メインループ
        switch(func_state){
            case 0:
				load();
			case 1:
				firstini();
				func_state=90;
				break;
			case 90:
				op1();
				break;
			case 99:
				op2();
				break;
            case 100:
				menu();
                break;
			case 101:
				ini();
				break;
			case 110:
				game();
				break;
			case 120:
				end();
				break;
            default:
                printfDx("不明なfunc_state\n");
                break;
        }
//		debug();
        if(CheckStateKey(KEY_INPUT_ESCAPE)==1)break;//エスケープが入力されたらブレイク
        ScreenFlip();//裏画面反映
		count++;
    }

    DxLib_End();//ＤＸライブラリ終了処理
    return 0;
}