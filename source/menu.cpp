#include "GV.h"

void nanido(){
	DrawExtendGraph( 0 , 0 , 640 , 480 , img_board[0] , FALSE ) ;
	
	DrawRotaGraph( 320 , 70 , 0.5f , 0 , img_board[3]  , TRUE ) ;
	DrawRotaGraph( 320 , 210 , 0.5f , 0 , img_board[4]  , TRUE ) ;
	DrawRotaGraph( 320 , 310 , 0.5f , 0 , img_board[5]  , TRUE ) ;
	DrawRotaGraph( 320 , 410 , 0.5f , 0 , img_board[6]  , TRUE ) ;


//	DrawBox( 205 , 177 , 435 , 243 , color[0] , FALSE) ;

	//クリック
	if(CheckStateMouse(MouseLeft)==1){
		int i;
		//位置確認
		for(i=0;i<3;i++){
			if(Mousesquare(205 , 177+(i*100) , 435 , 243+(i*100))==1){
				PlaySoundMem( sound_se[1] , DX_PLAYTYPE_BACK ) ;
				menu_state=1;
				level= i+1;
				selector=0;
				pointer=0;
			}
		}
	}

}

//画像選択
void select(){
	int i;

	DrawExtendGraph( 0 , 0 , 640 , 480 , img_board[0] , FALSE ) ;
	DrawRotaGraph( 320 , 70 , 0.5f , 0 , img_board[7]  , TRUE ) ;
	DrawRotaGraph( 100 , 420 , 0.5f , 0 , img_board[8]  , TRUE ) ;
	DrawRotaGraph( 250 , 420 , 0.5f , 0 , img_board[9]  , TRUE ) ;

	//ウィンドウ
	SetDrawBlendMode( DX_BLENDMODE_ALPHA , 180 ) ;
	DrawExtendGraph( 350 , 130 , 640 , 470 , img_board[11] , TRUE ) ;
	SetDrawBlendMode( DX_BLENDMODE_NOBLEND , 0 ) ;
	DrawExtendGraph( 350 , 130 , 640 , 470 , img_board[10] , TRUE ) ;

	//矢印
	DrawRotaGraph( 600 , 160 , 1.0f , 0 , img_board[12]  , TRUE ) ;
	DrawRotaGraph( 600 , 420 , 1.0f , PI , img_board[12]  , TRUE ) ;
	
	//サンプル画像
	DrawExtendGraph( 30 , 130 , 330 , 300 , graphfile[pointer] , TRUE ) ;


	//選択バー
	if(selector<=pointer &&  pointer<selector+10){
		SetDrawBlendMode( DX_BLENDMODE_ALPHA , 180 ) ;
		DrawBox( 370 , 145+(pointer-selector)*30 , 580 , 170+(pointer-selector)*30, color[2] , TRUE) ;
		SetDrawBlendMode( DX_BLENDMODE_NOBLEND , 0 ) ;
	}

	SetFontSize( 24 ) ;
	for(i=0;i<10;i++){
		DrawFormatString( 370 , 145+(i*30) ,color[1], "%s" , showname[selector+i] );
	}

	DrawFormatString( 110 , 330 ,color[3], "%s" , showname[pointer] );
	SetFontSize( FontSize ) ;



	//	DrawBox( 34 , 387 , 166 ,453 , color[0] , FALSE) ;
	//DrawRotaGraph( 250 , 420 , 0.5f , 0 , img_board[9]  , TRUE ) ;


	//クリック
	if(CheckStateMouse(MouseLeft)==1){
		int i;
		//位置確認
		for(i=0;i<10;i++){//ファイル名
			if(Mousesquare(370 , 145+(i*30) , 580 , 170+(i*30))==1){
				PlaySoundMem( sound_se[1] , DX_PLAYTYPE_BACK ) ;
				pointer=i+selector;
			}
		}
		//矢印
		if(Mousesquare(575 , 135 , 625 ,185)==1){
			PlaySoundMem( sound_se[1] , DX_PLAYTYPE_BACK ) ;
			if(selector!=0){
				selector-=1;
			}
		}
		if(Mousesquare(575 , 395 , 625 ,445)==1){
			PlaySoundMem( sound_se[1] , DX_PLAYTYPE_BACK ) ;
			if(selector!=gnum-10){
				selector+=1;
			}
		}
		//決定
		if(Mousesquare(34 , 387 , 166 ,453)==1){
			PlaySoundMem( sound_se[1] , DX_PLAYTYPE_BACK ) ;
			func_state=101;
		}
		//戻る
		if(Mousesquare(184 , 387 , 316 ,453)==1){
			PlaySoundMem( sound_se[1] , DX_PLAYTYPE_BACK ) ;
			menu_state=0;
		}
	}
	
}

void menu(){
	switch(menu_state){
	case 0://難易度
		nanido();
		break;
	case 1://ファイル指定
		select();
		break;

	}
}

