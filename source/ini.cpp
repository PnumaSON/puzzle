#include "GV.h"


void firstini(){

	color[0]=GetColor( 255 , 255 , 255 ) ;
	color[1]=GetColor( 0 , 0 , 0 ) ;
	color[2]=GetColor( 255 , 0 , 0 ) ;
	color[3]=GetColor( 220,220,0 ) ;
	color[9]=GetColor( 255,73,53 ) ;
	selector=0;
	pointer=0;
	menu_state=0;
	game_state=0;
	srand((unsigned int)time(NULL));
}

//ゲーム開始のイニテイト
void ini(){

	int GSizeX,GSizeY;
	int i,r,m;
	char tmp[100];

	//グラフィックのサイズ
	GetGraphSize( graphfile[pointer] , &GSizeX , &GSizeY ) ;

	switch(level){
	case 1:
		divnum=4;
		break;
	case 2:
		divnum=6;
		break;
	case 3:
		divnum=8;
		break;
	}
	//分割読み込み
	sprintf(tmp, "./graphic/%s", filename[pointer] ); 
	LoadDivGraph( tmp , divnum*divnum , divnum , divnum , GSizeX/divnum , GSizeY/divnum , puzzlegraph ) ;


	//数字の用意
	for (i = 0; i < divnum*divnum; i++) {
		puzzlenum[i]=i;
    }
	//シャッフル
	for (i = 0; i < divnum*divnum-1; i++) {
        r = rand()%(divnum*divnum-1);
        m = puzzlenum[i];
        puzzlenum[i] = puzzlenum[r];
        puzzlenum[r] = m;
    }

	
	space=divnum*divnum;
	func_state=110;

}