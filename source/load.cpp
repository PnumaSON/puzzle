#include "GV.h"


void fileload(){

    WIN32_FIND_DATA lp;
    HANDLE h;
	int i=0,j=0;
	char tmp[200];


	for(j=0;j<2;j++){
		if(j==0){
			h=FindFirstFile("graphic/*.png",&lp);
		}else{
			h=FindFirstFile("graphic/*.jpg",&lp);
		}
		/* 検索失敗? */
		if(h == INVALID_HANDLE_VALUE) {
			printf("検索失敗\n");
			return; /******** エラー終了 ********/
		}
		
		do{
			//ファイルの名前を記憶
			sprintf(tmp, "./graphic/%s", lp.cFileName ); 
			graphfile[i] = LoadGraph(tmp);
			strcpy(filename[i],lp.cFileName);
			strncpy(showname[i],lp.cFileName,17);
			if(strlen(lp.cFileName)>16){
				showname[i][16]='\0';
			}

			i++;
		}while(FindNextFile(h, &lp));

	}

	//グラフィックの数を記録
	gnum=i;

}



void load(){

	img_board[0] = LoadGraph("data/back.png");
	img_board[1] = LoadGraph("data/title.png");
	img_board[2] = LoadGraph("data/start.png");

	img_board[3] = LoadGraph("data/nanido.png");
	img_board[4] = LoadGraph("data/level1.png");
	img_board[5] = LoadGraph("data/level2.png");
	img_board[6] = LoadGraph("data/level3.png");

	img_board[7] = LoadGraph("data/gazo.png");
	img_board[8] = LoadGraph("data/ok.png");
	img_board[9] = LoadGraph("data/modoru.png");

	img_board[10] = LoadGraph("data/messe.png");
	img_board[11] = LoadGraph("data/messe2.png");
	img_board[12] = LoadGraph("data/yaji.png");

	img_board[13] = LoadGraph("data/moto.png");
	img_board[14] = LoadGraph("data/hint.png");
	img_board[15] = LoadGraph("data/lose.png");
	img_board[16] = LoadGraph("data/complete.png");
	img_board[17] = LoadGraph("data/complete2.png");


	sound_se[0]=LoadSoundMem("data/shu.wav");
	sound_se[1]=LoadSoundMem("data/pepepin.wav");

	fileload();
}