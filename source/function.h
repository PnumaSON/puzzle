//key.cpp
	//現在のキー入力処理を行う
	GLOBAL int GetHitKeyStateAll_2();
	//受け取ったキー番号の現在の入力状態を返す
	GLOBAL int CheckStateKey(unsigned char Handle);

	//現在のパッド入力処理を行う関数
	GLOBAL void GetHitPadStateAll();
	//受け取ったパッド番号の現在の入力状態を返す
	GLOBAL int CheckStatePad(unsigned int Handle);
	//マウスの入力処理
	GLOBAL void GetMouseState();
	GLOBAL int CheckStateMouse(int Handle);
	GLOBAL int CheckHitNowKeyAll();
	GLOBAL int Mousesquare(int x1,int y1,int x2, int y2);


	GLOBAL void firstini();
	GLOBAL void ini();
	GLOBAL void load();
	GLOBAL void graph();
	GLOBAL void op1();
	GLOBAL void op2();
	GLOBAL void end();
	GLOBAL void menu();
	GLOBAL void game();
	GLOBAL void debug();
