パズルゲー　ver１　
製作者　PnumaSON

このくだらないゲームをダウンロードいただき大変恐縮です。
こころを広く保ちながら、罵倒しつつゲームしていただければ光栄です。

クリアしたからといってなにか起こるわけではありませんのでご了承ください

著作権等
PNG読み込みによるライセンス
　　　libpng　Copyright (c) 1998-2011 Glenn Randers-Pehrson.
　　　zlib　Copyright (C) 1995-2010 Jean-loup Gailly and Mark Adler.
DXライブラリ
　　　DX Library Copyright (C) 2001-2013 Takumi Yamada.